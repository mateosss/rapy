# Informe - Rapy

# Decisiones de diseño

Si bien el marco de decisiones principal ya venía dado por el modelo MVC que
estábamos siguiendo, tuvimos que decidir sobre ciertos aspectos del cómo se
usarían ciertas funcionalidades.

Algo a tener en cuenta durante todo el proyecto, es que hay más cuidado por la
legibilidad declarativa que por la performance, no se cachea practicamente nada,
y no nos preocupamos de que funciones como `exist`, `get`, etc tengan que
recorrer repetidas veces los modelos. Si se lee bien, se queda.

Una de las decisiones, fue utilizar muchas precondiciones (`// Pre:`). Mientras
que el que llame a cada función, respete su precondición, no habrá problemas.
Esto se puede ver por ejemplo en `ModelCompanion`, donde tuvimos que elegir si
hacer todos los métodos *"seguros"* utilizando la misma idea de `subsetOf` que
se utiliza en el último `filter`. Pero decidimos que lo más sano, para evitar
errores silenciosos y permitir accessos más transparentes (ver función `got`),
se lograría con precondiciones.

Hay otras precondiciones a lo largo del código como por ejemplo en la api:

```scala
// Pre: All the instances referenced by id in an order still exist
@get("/api/orders/detail/:id")
```

Que muestra que orders se rompe si se eliminan items de una order ya realizada.
No nos preocupamos demasiado por esto por que preguntamos antes a los profesores
como proseguir. En otros ámbitos, la mejor forma de modelar un sistema de
registro de órdenes cómo este sería copiando todos los datos que pueden cambiar
en el futuro como consumer/provider/items names, precios de los items, etc.

El principal problema de diseño al que hubo que enfrentarse fue a cómo
implementar las clases:

```scala
class User
class Provider extends User
class Consumer extends User
```

Nuestra primera aproximación fue hacer un `class User extends Mode[User]`, pero
nos dimos cuenta que no iba a funcionar por las restricciones de tipo de `Model`
(en particular por `self: M`), aunque no pudimos resolverlo como quisimos en el
camino se aprendió mucho sobre el sistema de tipos de scala.

Lo que terminamos haciendo fue hacer todo sin `User`, y al final, intentar
arreglar la duplicación de código que había en la api sobre `Consumer` y
`Provider`. Para esto comenzamos implementando una especie de `ModelCompanion`:
`object User` que implementó la función `find` y `exists` que eran muy
necesarias en la api para tratar con `User`s. Luego pudimos abstraer un poco más
creando un `trait User` que define las 3 variables que definen a un usuario
incluyendo el manejo del `balance`.

Si bien la implementación de `User` no quedo tan limpia como nos hubiese
gustado, la interfaz que queda para el que la usa es suficientemente buena. En
retrospectiva, podría haberse solucionado haciendo que `Provider` y `Consumer`
tengan un miembro de tipo `User`, que actúa como una `ForeignKey` one-to-one en
vez de con herencia.


# Orientación a Objetos


## Encapsulamiento

Hicimos de sólo lectura todo lo que se pudo con `val`, y para las pocas
variables que guardan estado (`Order.status` y `User.balance`), las hicimos
`protected` para que no puedan ser modificadas salvo por como nosotros le
permitamos al usuario de la clase (con `Order.deliver()` y `User.addBalance()`)
y agregamos una función pública del mismo nombre para su lectura.


## Clases y Traits

Para mostrar que `Provider` y `Consumer` son usuarios, hicimos el `trait User`.
El resto del proyecto implementa principalmente `Model`, `ModelCompanion` y
hereda `DatabaseTable`.


## Sobrecarga

De operadores propiamente sólo el apply de los `ModelCompanion`, que por un
lado permite crear un nuevo objeto para guardar en la base de datos y por otro
permite cargar uno desde un `jsonValue` cargado de disco.

De funciones en general, le dimos uso en `ModelCompanion` para simplificar
la sintaxis de `filter` y `got`.


## Polimorfismo

Un lugar claro donde usamos el polimorfismo de `User` es en `usersDelete` de la
api, que utiliza `User.find()` el cual busca en `Consumer` o `Provider` y lo
devuelve como si fuera un `User` y eso permite luego hacerle un `user.delete()`
sin importar que tipo de user realmente es.

Por otro fue muy interesante trabajar con los tipos polimórficos de scala, cosas
como `Model[M <: Model[M]]` o `DatabaseTable[M]` probaron ser muy útiles. De
no haberlos tenido habría que implementar a mano cada DatabaseTable por cada
tipo.


# Conclusión

Scala nos resultó muy facil de aprender y divertido de usar, mezcla muy bien
conceptos funcionales con imperativos.

No nos esperábamos que tuviera tanto sentido en algo tan sencillo como una api
rest. Fue interesante ver cómo métodos GET son representados con funciones
puramente declarativas, mientras que en los POST, que justamente modifican el
estado, es muy dificil escaparse de chequeos imperativos, por suerte Scala nos
deja pasar de un paradigma al otro sin problemas cuando tiene sentido.
