# Grupo 25		
## Corrección		
	Tag o commit corregido:	lab-2
		
### Entrega y git		88,00%
	Informe	100,00%
	Commits de cada integrante	40,00%
	En tiempo y con tag correcto	100,00%
	Commits frecuentes y con nombres significativos	100,00%
### Funcionalidad		100,00%
	Se puede compilar y correr el servidor	100,00%
	Se pueden crear y recuperar usuarios	100,00%
	Se pueden crear y recuperar items	100,00%
	Se pueden crear y recuperar orders	100,00%
	Guardan los objetos luego de crearlos y actualizarlos	100,00%
### Modularización y diseño		100,00%
	Respetaron la estructura original del código	100,00%
	En los controladores sólo hay código de validación de errores, i.e. no hay lógica de los objetos	100,00%
	La mayor parte de la funcionalidad está en la clase y no en el object companion	100,00%
	Uso de métodos heredados con super, por ejemplo en toMap	100,00%
	Los usuarios actualizan su propio balance	100,00%
	Las órdenes actualizan su propio estado	100,00%
### Calidad de código		100,00%
	Buenas prácticas funcionales	100,00%
	Líneas de más de 120 caracteres	100,00%
	Estilo de código	100,00%
	Estructuras de código simples	100,00%
### Opcionales		
	Puntos estrella	
		
# Nota Final		9,784
		
		
# Comentarios		
	- Hay más commits de Mateo que de Mayco y Catriel juntos.	
