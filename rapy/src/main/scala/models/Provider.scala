package models

object Provider extends ModelCompanion[Provider] {
  protected def dbTable: DatabaseTable[Provider] = Database.providers

  // Pre: A Location with locationName exists
  def apply(
    username: String, storeName: String, locationName: String,
    maxDeliveryDistance: Int
  ): Provider = new Provider(
    username, Location.got("name", locationName).id, storeName,
    maxDeliveryDistance
  )

  private[models] def apply(jsonValue: JValue): Provider = {
    val value = jsonValue.extract[Provider]
    value._id = (jsonValue \ "id").extract[Int]
    value.save()
    value
  }
}

// Pre: A Location with locationId exists, a User with username does not
// Pre: and a Provider with the same storeName neither
class Provider(
  val username: String,
  val locationId: Int,
  val storeName: String,
  val maxDeliveryDistance: Int,
  protected var _balance: Float = 0
) extends Model[Provider] with User {
  protected def dbTable: DatabaseTable[Provider] = Provider.dbTable

  override def toMap: Map[String, Any] = super.toMap ++ Map(
    "username" -> username, "locationId" -> locationId,
    "storeName" -> storeName, "maxDeliveryDistance" -> maxDeliveryDistance,
    "balance" -> balance
  )

  def sellsItemWithName(itemName: String): Boolean =
    Item.filter(Map("providerId" -> this.id, "name" -> itemName)).nonEmpty

  override def toString: String = s"Provider: $username of Store: $storeName"
}
