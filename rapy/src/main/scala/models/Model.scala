package models

trait ModelCompanion[M <: Model[M]] {
  protected def dbTable: DatabaseTable[M]

  private[models] def apply(jsonValue: JValue): M

  def all: List[M] = dbTable.instances.values.toList

  def get(id: Int): Option[M] = dbTable.instances.get(id)

  // Pre: key is an attribute of M
  def find(key: String, value: Any): Option[M] =
    dbTable.instances.values find (_.toMap(key) == value)

  // Pre: M with id=id exists
  def got(id: Int): M = dbTable.instances(id)

  // Pre: M with key=value exists
  def got(key: String, value: Any): M = filter(key, value)(0)

  // Pre: key is an attribute of M
  def exists(key: String, value: Any): Boolean =
    dbTable.instances.values exists (_.toMap(key) == value)

  // Pre: key is a valid attribute of M
  def filter(key: String, value: Any): List[M] =
    all filter (_.toMap(key) == value)

  def filter(mapOfAttributes: Map[String, Any]): List[M] =
    all filter (mapOfAttributes.toSet subsetOf _.toMap.toSet)

}

trait Model[M <: Model[M]] { self: M =>
  protected var _id: Int = 0

  def id: Int = _id

  protected def dbTable: DatabaseTable[M]

  def toMap: Map[String, Any] = Map("id" -> _id)

  def save(): Unit = {
    if (_id == 0) _id = dbTable.getNextId
    dbTable.save(this)
  }

  def delete(): Unit = dbTable.delete(this)
}
