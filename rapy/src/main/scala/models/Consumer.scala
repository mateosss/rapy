package models

object Consumer extends ModelCompanion[Consumer] {
  protected def dbTable: DatabaseTable[Consumer] = Database.consumers

  // Pre: A Location with locationName exists
  def apply(username: String, locationName: String): Consumer =
    new Consumer(username, Location.got("name", locationName).id)

  private[models] def apply(jsonValue: JValue): Consumer = {
    val value = jsonValue.extract[Consumer]
    value._id = (jsonValue \ "id").extract[Int]
    value.save()
    value
  }
}

// Pre: A Location with locationId exists
class Consumer(
  val username: String,
  val locationId: Int,
  protected var _balance: Float = 0
) extends Model[Consumer] with User {
  protected def dbTable: DatabaseTable[Consumer] = Consumer.dbTable

  override def toMap: Map[String, Any] = super.toMap ++ Map(
    "username" -> username, "locationId" -> locationId, "balance" -> balance
  )

  override def toString: String = s"Consumer: $username"
}
