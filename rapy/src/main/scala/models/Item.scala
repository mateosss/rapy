package models

import upickle.default.{ReadWriter, macroRW}

object ItemParam { implicit val rw: ReadWriter[ItemParam] = macroRW }
case class ItemParam(val name: String, val amount: Int)

object Item extends ModelCompanion[Item] {
  protected def dbTable: DatabaseTable[Item] = Database.items

  // Pre: A Provider with providerUsername exists
  def apply(
    name: String, description: String, price: Float, providerUsername: String
  ): Item = new Item(
    name, description, price, Provider.got("username", providerUsername).id
  )

  private[models] def apply(jsonValue: JValue): Item = {
    val value = jsonValue.extract[Item]
    value._id = (jsonValue \ "id").extract[Int]
    value.save()
    value
  }
}

// Pre: A Provider with providerId exists
class Item(
  val name: String,
  val description: String,
  val price: Float,
  val providerId: Int
) extends Model[Item] {
  protected def dbTable: DatabaseTable[Item] = Item.dbTable

  override def toMap: Map[String, Any] = super.toMap ++ Map(
    "name" -> name, "price" -> price, "description" -> description,
    "providerId" -> providerId
  )

  override def toString: String = s"Item: $name"
}
