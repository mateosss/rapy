package models

// Utility singleton that improves readability, but doesn't completely solve
// the inheritance design flaw in the Consumer and Provider classes
object User {
  // Pre: key is an attribute of User
  def exists(key: String, value: Any): Boolean =
    Consumer.exists(key, value) || Provider.exists(key, value)

  // Pre: key is an attribute of User
  def find(key: String, value: Any): Option[User] =
    Consumer.find(key, value) match {
      case None => Provider.find(key, value)
      case consumer => consumer
    }
}

trait User {
  val username: String
  val locationId: Int
  protected var _balance: Float

  def balance: Float = _balance

  def save(): Unit
  def delete(): Unit

  def addBalance(balance: Float): Unit = {
    _balance += balance
    save()
  }
}
