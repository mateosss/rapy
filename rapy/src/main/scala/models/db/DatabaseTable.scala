package models.db

import scala.collection.mutable.{Map => Dict}

import models.Model

class DatabaseTable[M <: Model[M]](val filename: String) {
  private val _instances: Dict[Int, M] = Dict()

  private[models] def getNextId: Int =
    if (_instances.isEmpty) 1 else _instances.keys.max + 1

  def instances: Map[Int, M] = _instances.toMap

  def delete(instance: M): Unit = {
    _instances.remove(instance.id)
    Database.saveDatabaseTable(this)
  }

  def save(instance: M): Unit = {
    _instances(instance.id) = instance
    Database.saveDatabaseTable(this)
  }
}
