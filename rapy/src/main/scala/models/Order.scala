package models

object Order extends ModelCompanion[Order] {
  protected def dbTable: DatabaseTable[Order] = Database.orders

  def apply(consumerId: Int, providerId: Int, items: Map[Int, Int]): Order =
    new Order(consumerId, providerId, items)

  private[models] def apply(jsonValue: JValue): Order = {
    val value = jsonValue.extract[Order]
    value._id = (jsonValue \ "id").extract[Int]
    value.save()
    value
  }
}

// Pre: A consumer, provider and all the items with their respective ids exist
class Order(
  val consumerId: Int,
  val providerId: Int,
  val items: Map[Int, Int], // Map(itemId, quantity)
  protected var _status: Int = 0 // 0: Payed, 1: Delivered
) extends Model[Order] {

  protected def dbTable: DatabaseTable[Order] = Order.dbTable

  def status: Int = _status

  override def toMap: Map[String, Any] = super.toMap ++ Map(
    "consumerId" -> consumerId,
    "providerId" -> providerId,
    "items" -> items,
    "orderTotal" -> orderTotal,
    "status" -> status
  )

  def toResponseMap: Map[String, Any] = Map(
    "id" -> id,
    "orderTotal" -> orderTotal,
    "status" -> Map(0 -> "payed", 1 -> "delivered")(status),
    "consumerId" -> consumerId,
    "providerId" -> providerId,
  ) + (
    "providerStoreName" -> Provider.got(providerId).storeName
  ) ++ ((consumer: Consumer) => Map(
    "consumerUsername" -> consumer.username,
    "consumerLocation" -> Location.got(consumer.locationId).name
  )) (Consumer.got(consumerId))

  def deliver(): Unit = {
    if (status == 1) return
    _status = 1
    updateBalances()
    save()
  }

  def isDelivered(): Boolean = status == 1;

  def orderTotal: Float = (items map {
    case (id, amount) => Item.filter(Map("id" -> id))(0).price -> amount
  }).foldLeft(0.0f) { case (a, e) => a + e._1 * e._2 }

  // Pre: provider/consumer with providerId/consumerId exists
  def updateBalances(): Unit = {
    Provider.got(providerId).addBalance(+orderTotal)
    Consumer.got(consumerId).addBalance(-orderTotal)
  }

  override def toString: String = (
    s"Order: consumerId=$consumerId bought ${items.values.sum} items from " +
    s"providerId=$providerId"
  )
}
