package app

import cask._
import models._

object RestfulAPIServer extends MainRoutes  {
  override def host: String = "0.0.0.0"
  override def port: Int = 4000

  // Root

  @get("/")
  def root(): Response = JSONResponse("Ok")

  // Locations

  @get("/api/locations")
  def locations(): Response = JSONResponse(Location.all map (_.toMap))

  @postJson("/api/locations")
  def locations(name: String, coordX: Int, coordY: Int): Response = {
    if (Location.exists("name", name))
      return JSONResponse("Existing location name", 409)

    val location = Location(name, coordX, coordY)
    location.save()
    JSONResponse(location.id)
  }

  // Consumers

  @get("/api/consumers")
  def consumers(): Response = JSONResponse(Consumer.all map (_.toMap))

  @postJson("/api/consumers")
  def consumers(username: String, locationName: String): Response = {
    if (User.exists("username", username))
      return JSONResponse("Existing username", 409)

    if (!Location.exists("name", locationName))
      return JSONResponse("Non existing location", 404)

    val consumer = Consumer(username, locationName)
    consumer.save()
    JSONResponse(consumer.id)
  }

  // Providers

  @get("/api/providers")
  def providers(locationName: String = ""): Response = locationName match {
    case "" => JSONResponse(Provider.all map (_.toMap))
    case _ => Location.find("name", locationName) match {
      case None => JSONResponse("Non existing location", 404)
      case Some(location) => JSONResponse(
        Provider.filter("locationId", location.id) map (_.toMap)
      )
    }
  }

  @postJson("/api/providers")
  def providers(
    username: String, storeName: String, locationName: String,
    maxDeliveryDistance: Int
  ): Response = {
    if (maxDeliveryDistance < 0)
      return JSONResponse("Negative maxDeliveryDistance", 400)

    if (User.exists("username", username))
      return JSONResponse("Existing username", 409)

    if (Provider.exists("storeName", storeName))
      return JSONResponse("Existing storeName", 409)

    if (!Location.exists("name", locationName))
      return JSONResponse("Non existing location", 404)

    val provider =
      Provider(username, storeName, locationName, maxDeliveryDistance)
    provider.save()
    JSONResponse(provider.id)
  }

  // Users

  @post("/api/users/delete/:username")
  def usersDelete(username: String): Response =
    User.find("username", username) match {
      case None => JSONResponse("Non existing user", 404)
      case Some(user) => { user.delete(); JSONResponse("Ok") }
    }

  // Items

  @get("/api/items")
  def items(providerUsername: String = ""): Response = providerUsername match {
    case "" => JSONResponse(Item.all map (_.toMap))
    case _ => Provider.find("username", providerUsername) match {
      case None => JSONResponse("Non existing provider", 404)
      case Some(provider) => JSONResponse(
        Item.filter("providerId", provider.id) map (_.toMap)
      )
    }
  }

  @postJson("/api/items")
  def items(
    name: String, description: String, price: Float, providerUsername: String
  ): Response = {
    if (price < 0)
      return JSONResponse("Negative price", 400)

    val providers = Provider.filter("username", providerUsername)
    if (providers.isEmpty)
      return JSONResponse("Non existing provider", 404)
    val providerId = providers(0).id

    if (Item.filter(Map("providerId" -> providerId, "name" -> name)).nonEmpty)
      return JSONResponse("Existing item for provider", 409)

    val item = Item(name, description, price, providerUsername)
    item.save()
    JSONResponse(item.id)
  }

  @post("/api/items/delete/:id")
  def itemsDelete(id: Int): Response = Item.get(id) match {
    case None => JSONResponse("Non existing item", 404)
    case Some(item) => { item.delete(); JSONResponse("Ok") }
  }

  // Orders

  @get("/api/orders")
  def orders(username: String = ""): Response = username match {
    case "" => JSONResponse(Order.all map (_.toResponseMap))
    case _ => User.find("username", username) match {
      case None => JSONResponse("Non existing user", 404)
      case Some(consumer: Consumer) => JSONResponse(
        Order.filter("consumerId", consumer.id) map (_.toResponseMap)
      )
      case Some(provider: Provider) => JSONResponse(
        Order.filter("providerId", provider.id) map (_.toResponseMap)
      )
      case _ => JSONResponse("Non supported kind of user exists", 500)
    }
  }

  // Pre: All the instances referenced by id in an order still exist
  @get("/api/orders/detail/:id")
  def ordersDetail(id: Int): Response = Order.get(id) match {
    case None => JSONResponse("Non existing order", 404)
    case Some(order) => JSONResponse(order.items map {
      case (id, amount) => Item.got(id).toMap + ("amount" -> amount)
    })
  }

  // Pre: there aren't two items with the same id in items
  @postJson("/api/orders")
  def orders(
    providerUsername: String, consumerUsername: String, items: Seq[ItemParam]
  ): Response = {
    if (items exists (_.amount < 0))
      return JSONResponse("Negative amount", 400)

    val providers = Provider.filter("username", providerUsername)
    if (providers.isEmpty)
      return JSONResponse("Non existing provider", 404)
    val provider = providers(0)
    val providerId = provider.id

    val consumers = Consumer.filter("username", consumerUsername)
    if (consumers.isEmpty)
      return JSONResponse("Non existing consumer", 404)
    val consumerId = consumers(0).id

    if (items exists (item => !provider.sellsItemWithName(item.name)))
      return JSONResponse("Non existing item for provider", 404)

    val itemsIdAmount: Map[Int, Int] = Map() ++ (items map (item =>
      Item.filter(Map(
        "providerId" -> providerId, "name" -> item.name
      ))(0).id -> item.amount
    ))

    val order = Order(consumerId, providerId, itemsIdAmount)
    order.save()
    JSONResponse(order.id)
  }

  @post("/api/orders/delete/:id")
  def ordersDelete(id: Int): Response = Order.get(id) match {
    case None => JSONResponse("Non existing order", 404)
    case Some(order) => { order.delete(); JSONResponse("Ok") }
  }

  @post("/api/orders/deliver/:id")
  def ordersDeliver(id: Int): Response = Order.get(id) match {
    case None => JSONResponse("Non existing order", 404)
    case Some(order) => {
      if (order.isDelivered) {
        JSONResponse("Already delivered", 400)
      } else {
        order.deliver();
        JSONResponse("Ok")
      }
    }
  }

  override def main(args: Array[String]): Unit = {
    System.err.println("\n " + "=" * 39)
    System.err.println(s"| Server running at http://$host:$port ")

    if (args.length > 0) {
      val databaseDir = args(0)
      Database.loadDatabase(databaseDir)
      System.err.println(s"| Using database directory $databaseDir ")
    } else {
      Database.loadDatabase()  // Use default location
    }
    System.err.println(" " + "=" * 39 + "\n")

    super.main(args)
  }

  initialize()
}
